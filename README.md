# camping

Manifest for my camping/survival adventures

## Table of contents


<!-- vim-markdown-toc GitLab -->

* [Carry list](#carry-list)
	* [Storage](#storage)
		* [Backpack](#backpack)
		* [Waterproof storage](#waterproof-storage)
	* [Shelter](#shelter)
		* [Clothes](#clothes)
		* [Sleeping system](#sleeping-system)
	* [Water system](#water-system)
		* [Filters](#filters)
		* [Boiling water](#boiling-water)
		* [Chemical puricication](#chemical-puricication)
	* [Food and cooking system](#food-and-cooking-system)
		* [Consumables](#consumables)
			* [Ingredients](#ingredients)
			* [Fuel](#fuel)
		* [Cooking system](#cooking-system)
			* [Cutlery](#cutlery)
			* [Heat producing](#heat-producing)
			* [Fire making](#fire-making)
			* [Maintnance](#maintnance)
	* [Light](#light)
		* [Environmental light](#environmental-light)
		* [Handheld light](#handheld-light)
		* [Headlamps](#headlamps)
	* [Tools](#tools)
		* [Multitools](#multitools)
		* [Bushcraft](#bushcraft)
	* [Navigation](#navigation)
		* [Electronics](#electronics)
		* [Old school](#old-school)
	* [Entratainment](#entratainment)
		* [Electronics](#electronics-1)
	* [Environmental protection](#environmental-protection)
	* [Health](#health)
		* [Medicine](#medicine)
	* [Hygiene](#hygiene)
		* [Toilet](#toilet)
		* [Washing](#washing)
		* [Digging](#digging)
		* [Toiletries](#toiletries)
	* [Survival (emergency)](#survival-emergency)
	* [Maintnance and batteries](#maintnance-and-batteries)
		* [Sharpening tools](#sharpening-tools)
		* [Batteries](#batteries)

<!-- vim-markdown-toc -->

## Carry list

This is list of items, sorted by their priority and with maintnance notes.

### Storage

#### Backpack

#### Waterproof storage

* Waterproof rollup bag

### Shelter

#### Clothes

#### Sleeping system

* Tent

	* Tent stakes

	* Additional cordage

	* Tent repair kit

* Sleeping pad

	* Sleeping pad repair kit

* Sleeping bag

	* Earplugs

	* Eye cover

### Water system

With water filtering, it is good idea to combine different purification method.

For example, it is always a good idea to use handkerchief as a prefilter. 

#### Filters

* Sawyer mini filter

* Sawyer bottle

* Prefiltering handkerchief

#### Boiling water

* Metal, single walled bottle for water boiling

#### Chemical puricication

* Water purification tablets

### Food and cooking system

#### Consumables

##### Ingredients

##### Fuel

* Primus fuel

#### Cooking system

* Deep cooking pan/mug

* Shalow cooking pan

##### Cutlery

* Spork

* Knife

##### Heat producing

* Primus fuel burning stove

##### Fire making

* Firewood processing

	* Saw
	
	* Knife for making shavings

* Lighter

* Matches

* Ferro rod

##### Maintnance

* Sponge for cleaning

### Light

#### Environmental light

* Tent lantern

#### Handheld light

* Olight M2R Warrior Pro

* Olight M2R Warrior

#### Headlamps

* Petzl headlamp

### Tools

#### Multitools

* Leatherman mut

* Leatherman bit kit

#### Bushcraft

* Mora companion

* Folding saw

### Navigation

#### Electronics

* Garmin Tactix Delta

* Garmin GPSMAP 66i

* Phone

#### Old school

* Compass

### Entratainment

#### Electronics

* Kobo Aura One (ebook reader)

* FM Radio

### Environmental protection

Sun protection, insect protection, animal protection

### Health

#### Medicine

* Rupurut

* Brufen

### Hygiene

#### Toilet

#### Washing

* Wet wipes

#### Digging

* Folding showel

#### Toiletries

* Disinfection gel

* Toilet paper

### Survival (emergency)

* Signaling mirror

* Whistle

### Maintnance and batteries

#### Sharpening tools

* Leather strop

#### Batteries

* 20,000 Mah powerbank from Anker

* !0,000 Mah powerbank

